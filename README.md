# Tetris Haskell

| tetris-haskell   | 0.1.0.1                            |
| ---------------- | ---------------------------------- |
| Maintainer       | Allele Dev (allele.dev@gmail.com ) |
| Funding          | $0 USD                             |
| Copyright        | Copyright (C) 2017 Allele Dev      |
| License          | GPL-3                              |

An implementation of Tetris in Haskell.

## Features

Plenty planned, but this is only the beginning. Some hopefuls:

* swappable color palettes
* game modes: marathon, line clear, puzzle, etc.
* score tracking
* tracking lines cleared
* rebindable keys
* custom game grid sizes
* chains??
* sound effects??
* music??
* multiplayer??

## Contributing

Contributions are welcome! Documentation, examples, code, and
feedback - they all help.

Be sure to review the included code of conduct. This project adheres
to the [Contributor's Covenant](http://contributor-covenant.org/). By
participating in this project you agree to abide by its terms.

## Developer Setup

The easiest way to start contributing is to install
[stack](https://github.com/commercialhaskell/stack). stack can install
GHC/Haskell for you, and automates common developer tasks.

The key commands are:

* `stack setup`: install GHC
* `stack build`: build the project and (only once) all dependencies
* `stack clean`: clean the build
* `stack haddock`: builds documentation
* `stack test`: run all tests
    * `stack test tetris-haskell:tetris-haskell-test`: runs only spec & prop tests
    * `stack exec doctetsts`: run only doctest examples
    * `stack exec tetris-haskell-hlint`: lint source tree
* `stack bench`: run all benchmarks
    * `stack bench tettris-haskell:tetris-haskell-bench`: run only core benchmarks
* `stack ghci`: start a REPL instance

## Licensing

This project is distributed under the GPL-3 license. See the included
[LICENSE](./LICENSE) file for more details.
