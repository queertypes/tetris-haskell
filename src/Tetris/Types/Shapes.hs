module Tetris.Types.Shapes () where

import YNotPrelude
import Linear.V4

data TetronimoType
  = I -- long straight line
  | J -- 3 blocks and 1 block on bottom right
  | L -- 3 blocks and 1 block on bottom left
  | O -- a 2x2 square
  | S -- 2 block foundation, 2 blocks offset 1 right on top
  | T -- 3 blocks on top, 1 block below in the middle
  | Z -- 2 block foundation, 2 blocks offset 1 left on top

-- |
--
-- Assuming a grid with (X=0, Y=0) is the bottom left and each
-- tetronimo component square takes up a 1x1 area, below are the base
-- coordinates for all tetronimos.
--
-- I: [ ]  J:            L:
--    [ ]
--    [ ]     [ ][ ][ ]     [ ][ ][ ]
--    [ ]           [ ]     [ ]
--
-- O: [ ][ ]  S:    [ ][ ]  T: [ ][ ][ ]  Z: [ ][ ]
--    [ ][ ]     [ ][ ]           [ ]           [ ][ ]
baseCoords t =
  case t of
    I -> [(0,0), (0,1), (0,2), (0,3)]
    J -> [(0,1), (1,1), (2,1), (2,0)]
    L -> [(0,1), (1,1), (2,1), (0,0)]
    O -> [(0,0), (0,1), (1,0), (1,1)]
    S -> [(0,0), (1,0), (1,1), (2,1)]
    T -> [(0,1), (1,1), (2,1), (1,0)]
    Z -> [(2,0), (1,0), (1,1), (0,1)]

-- |
-- Rotations, CW:
--
-- I(0):   [ ]
--         [ ]
--         [ ]
--         [ ]
--
-- I(1):
--
--       [ ][ ][ ][ ]
--
--
-- I(2):      [ ]
--            [ ]
--            [ ]
--            [ ]
--
-- I(3):
--       [ ][ ][ ][ ]
--
--
--
-- S(0):    [ ][ ]
--       [ ][ ]
--
--
-- S(1):    [ ]
--          [ ][ ]
--             [ ]
--
-- S(2):    [ ][ ]
--       [ ][ ]
--
--
-- S(3):    [ ]
--          [ ][ ]
--             [ ]
--
rotateCW = undefined
